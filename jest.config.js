module.exports = {
  // ts-jest has problems finding rootDir and shows
  // a warning because of PNPM; this globals property
  // fixed the warning.
  globals: {
    "ts-jest": {
      packageJson: "package.json",
    },
  },

  roots: ["<rootDir>/src", "<rootDir>/test"],
  transform: {
    "^.+\\.tsx?$": "ts-jest",
  },

  setupFilesAfterEnv: ["jest-expect-message"],
};
