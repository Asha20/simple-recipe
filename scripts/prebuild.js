#!/usr/env node

const fs = require("fs");
const path = require("path");
const pkg = require("../package.json");

const ROOT = path.join(__dirname, "..");
const DIST = path.join(ROOT, "dist/");

function rmDir(dir) {
  if (!fs.existsSync(dir)) {
    return;
  }

  fs.readdirSync(dir).forEach(child => {
    const childPath = path.join(dir, child);
    if (fs.lstatSync(childPath).isDirectory()) {
      rmDir(childPath);
    } else {
      fs.unlinkSync(childPath);
    }
  });
  fs.rmdirSync(dir);
}

/**
 * Modifies the source code by embedding the current
 * application version from `package.json`. This is a sort
 * of an alternative to webpack's DefinePlugin since there's
 * no reason to include webpack for this simple functionality.
 */
function updateAppVersion() {
  const newVersion = JSON.stringify(pkg.version);

  const entry = path.resolve(__dirname, "..", "src", "simple-recipe.ts");
  const versionRegex = /^export const VERSION = .+;$/m;

  function exitWithMessage(msg, code = 1) {
    console.log(msg);
    process.exit(code);
  }

  fs.readFile(entry, "utf8", (err, data) => {
    if (err) {
      return exitWithMessage(err.message);
    }

    const newData = data.replace(
      versionRegex,
      `export const VERSION = ${newVersion};`,
    );

    fs.writeFile(entry, newData, err => {
      if (err) {
        return exitWithMessage(err.message);
      }
      process.exit(0);
    });
  });
}

updateAppVersion();
rmDir(DIST);
