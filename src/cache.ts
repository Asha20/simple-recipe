import { existsSync, readFileSync } from "fs";
import { writeFile } from "./fs-promise";
import { CONFIG, VERSION } from "./simple-recipe";

/**
 * Represents a generic cache. It stores hashes of generated recipes,
 * which are later used upon recompilation to avoid unnecessary
 * overwriting of recipes that haven't changed.
 */
export interface RecipeCache {
  /** In-memory cache representation. */
  local: {
    hashes: Record<string, string>;
    /** Version of the app that generated the cache. */
    appVersion: string;
    /** Version of the game for the cache. */
    gameVersion: string;
  };
  /** Commits local cache to a persistent storage. */
  save: () => Promise<void>;
}

function defaultCache(): RecipeCache["local"] {
  return {
    hashes: {},
    appVersion: VERSION,
    gameVersion: CONFIG.gameVersion,
  };
}

function shouldInvalidateCache(local: RecipeCache["local"]): boolean {
  return (
    local.gameVersion !== CONFIG.gameVersion || local.appVersion !== VERSION
  );
}

function readJSONSync(filename: string): unknown {
  return JSON.parse(readFileSync(filename, "utf8"));
}

/**
 * Stores cached recipes inside a provided file.
 */
export function fileCache(filename: string): RecipeCache {
  const local = existsSync(filename)
    ? (readJSONSync(filename) as RecipeCache["local"])
    : defaultCache();

  const cache: RecipeCache = {
    // Invalidate the cache if its version mismatches the application's version.
    local: shouldInvalidateCache(local) ? defaultCache() : local,
    save: () => writeFile(filename, JSON.stringify(cache.local, null, 4)),
  };
  return cache;
}

/**
 * Stores cached recipes in memory.
 */
export function memoryCache(): RecipeCache {
  const cache: RecipeCache = {
    local: defaultCache(),
    save: () => Promise.resolve(),
  };
  return cache;
}
