// https://en.wikipedia.org/wiki/S%C3%B8rensen%E2%80%93Dice_coefficient
function createPairs(str: string) {
  const pairs: string[] = [];
  for (let i = 0; i < str.length - 1; i++) {
    pairs.push(str[i] + str[i + 1]);
  }
  return pairs;
}

export function calculateSimilarity(s1: string, s2: string) {
  const pairs1 = createPairs(s1);
  const pairs2 = new Set(createPairs(s2));

  const intersectionCount = [...pairs1].filter(x => pairs2.has(x)).length;

  return (2 * intersectionCount) / (pairs1.length + pairs2.size);
}

export function findClosestMatches(
  haystack: string[],
  needle: string,
  maxResults: number,
  threshold: number,
) {
  return haystack
    .map(word => ({
      word,
      index: calculateSimilarity(needle, word),
    }))
    .sort((a, b) => b.index - a.index)
    .filter(x => x.index >= threshold)
    .slice(0, maxResults)
    .map(x => x.word);
}
