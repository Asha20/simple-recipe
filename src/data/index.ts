export const supportedVersions = ["1.14"];

export function isSupportedVersion(version: string) {
  return supportedVersions.includes(version);
}

interface ItemModule {
  items: Set<string>;
}

function getModule(version: string): ItemModule {
  switch (version) {
    case "1.14":
      return require("./1.14");
    default:
      throw new Error(
        `Version "${version}" is incompatible with this program.`,
      );
  }
}

export function getItems(version: string) {
  return getModule(version).items;
}
