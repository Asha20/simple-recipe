import fs from "fs";
import { promisify } from "util";

// fs.promises is experimental at the time of writing
// this code, so needed functions are promisified manually.

export const readFile = promisify(fs.readFile);
export const writeFile = promisify(fs.writeFile);

const _unlink = promisify(fs.unlink);
export async function unlink(path: string) {
  try {
    return await _unlink(path);
  } catch (e) {}
}

export function mkDir(path: string) {
  return fs.mkdir(path, err => {
    return err && err.code !== "EEXIST"
      ? Promise.reject(err)
      : Promise.resolve();
  });
}

export function rmDir(path: string) {
  return fs.rmdir(path, err => {
    return err && err.code !== "ENOENT"
      ? Promise.reject(err)
      : Promise.resolve();
  });
}

const _lstat = promisify(fs.lstat);
export async function isDir(path: string) {
  try {
    const stat = await _lstat(path);
    return stat.isDirectory();
  } catch (e) {
    return false;
  }
}
