import { CONFIG } from "./simple-recipe";

function timestamp() {
  const now = new Date();
  const hours = String(now.getHours()).padStart(2, "0");
  const minutes = String(now.getMinutes()).padStart(2, "0");
  const seconds = String(now.getSeconds()).padStart(2, "0");
  return "[" + hours + ":" + minutes + ":" + seconds + "] ";
}

function print(level: number, prefix = "") {
  return function _print(message: string) {
    if (level <= CONFIG.verbosity) {
      console.log(timestamp() + prefix + message);
    }
  };
}

export function clear() {
  console.clear();
}

export const log = print(3);
export const important = print(2);
export const error = print(1, "ERROR: ");
