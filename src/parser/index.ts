import { Either, either, isLeft, left } from "fp-ts/lib/Either";
import { flow } from "fp-ts/lib/function";
import * as YAML from "js-yaml";
import * as path from "path";
import { readFile } from "../fs-promise";
import { CONFIG } from "../simple-recipe";
import * as recipes from "./recipes";
import { BaseRecipe, Recipe } from "./recipes";
import { isObject } from "./util";

type AnyObject = Record<string, any>;

interface Meta {
  name?: string;
}

export interface RecipeError {
  origin: string;
  index: number;
  path: string[];
  name: string;
  message: string;
}

export function recipeError(
  message: string,
  path: string = "",
  origin: string = "",
  name: string = "",
  index: number = 0,
): RecipeError {
  return {
    message,
    path: path ? path.split(".") : [],
    origin,
    name,
    index,
  };
}

export const recipeErrorE = flow(
  recipeError,
  left,
);

function parseMeta(x: AnyObject): Meta {
  return Object.keys(x).reduce<AnyObject>((acc, key) => {
    if (key.startsWith("_")) {
      acc[key.slice(1)] = x[key];
    }
    return acc;
  }, {});
}

export type RecipeWithMeta = Recipe & {
  group?: string;
  meta: Meta;
};

type CreateRecipes<R extends Recipe> = R extends Recipe
  ? BaseRecipe<R["type"]>
  : never;

type AnyBaseRecipe = CreateRecipes<Recipe>;

function objectToRecipe(x: AnyBaseRecipe): Either<RecipeError, Recipe> {
  switch (x.type) {
    case "crafting_shaped":
      return recipes.parseShapedCrafting(x);
    case "crafting_shapeless":
      return recipes.parseShapelessCrafting(x);
    case "stonecutting":
      return recipes.parseStonecutting(x);
    case "blasting":
      return recipes.parseBlasting(x);
    case "campfire_cooking":
      return recipes.parseCampfireCooking(x);
    case "smelting":
      return recipes.parseSmelting(x);
    case "smoking":
      return recipes.parseSmoking(x);
    case "crafting_special_armordye":
    case "crafting_special_bannerduplicate":
    case "crafting_special_bookcloning":
    case "crafting_special_firework_rocket":
    case "crafting_special_firework_star":
    case "crafting_special_firework_star_fade":
    case "crafting_special_mapcloning":
    case "crafting_special_mapextending":
    case "crafting_special_repairitem":
    case "crafting_special_shielddecoration":
    case "crafting_special_shulkerboxcoloring":
    case "crafting_special_tippedarrow":
    case "crafting_special_suspiciousstew":
      return recipes.parseCraftingSpecial(x);
    default:
      return recipeErrorE("Unrecognized recipe type was provided.", "type");
  }
}

export function parseRecipe(
  x: unknown,
  fallbackName?: string,
): Either<RecipeError, RecipeWithMeta> {
  if (!isObject(x)) {
    return recipeErrorE("A recipe must be an object.");
  }

  if (x.type === undefined) {
    return recipeErrorE('All recipes must have a "type" property.', "type");
  }

  if ((x.group && typeof x.group !== "string") || x.group === "") {
    return recipeErrorE('"group" must be a non-empty string.', "group");
  }

  if (typeof x.type !== "string") {
    return recipeErrorE('"type" must be a string.', "type");
  }

  if (x._name !== undefined && typeof x._name !== "string") {
    return recipeErrorE("Expected a string.", "_name");
  }

  if (x._name === "") {
    return recipeErrorE(`Property may not be an empty string.`, "_name");
  }

  x._name = x._name || fallbackName || "";
  const name: string = x._name;
  const recipe = objectToRecipe(x as AnyBaseRecipe);

  const recipeWithMeta = either.map(recipe, rec => {
    const recWithMeta = rec as RecipeWithMeta;
    recWithMeta.meta = parseMeta(x);
    if (x.group) {
      recWithMeta.group = x.group;
    }
    return recWithMeta;
  });

  return either.mapLeft(recipeWithMeta, x => {
    x.name = name;
    return x;
  });
}

export function parseRecipes(
  xs: unknown,
): Either<RecipeError, RecipeWithMeta>[] {
  if (!Array.isArray(xs)) {
    return [recipeErrorE("Expected an array of recipes.")];
  }

  type ParsedRecipe = Either<RecipeError, RecipeWithMeta>;

  const getName = (x: ParsedRecipe) =>
    isLeft(x) ? x.left.name : x.right.meta.name!;

  const seenRecipes = new Set<string>();
  const duplicateRecipes = new Set<string>();

  const parsedRecipes = xs.map((x, i) => {
    const recipe = parseRecipe(x, String(i + 1));

    const name = getName(recipe);
    if (seenRecipes.has(name)) {
      duplicateRecipes.add(name);
    }
    seenRecipes.add(name);

    return either.mapLeft(recipe, rec => {
      rec.index = i + 1;
      return rec;
    });
  });

  return parsedRecipes.map((x, i) => {
    const name = getName(x);
    const index = i + 1;
    return duplicateRecipes.has(name)
      ? recipeErrorE(
          `Found duplicate recipes the same name.`,
          "",
          "",
          name,
          index,
        )
      : x;
  });
}

export async function parseFile(
  file: string,
): Promise<Either<RecipeError, RecipeWithMeta>[]> {
  let content: string;
  let yaml: any;

  try {
    content = await readFile(file, "utf8");
  } catch (e) {
    return [recipeErrorE(`Error while reading file: ${file}`)];
  }

  try {
    yaml = YAML.safeLoad(content);
  } catch (e) {
    return [recipeErrorE(`Error while parsing YAML: ${e.message}`)];
  }

  return parseRecipes(yaml).map(x =>
    either.mapLeft(x, err => {
      err.origin = path.relative(CONFIG.root, file);
      return err;
    }),
  );
}
