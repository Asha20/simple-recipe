import { ItemOrTag } from "./ItemOrTag";
import { Stack } from "./Stack";

export type ItemIngredient = { item: string };
export type TagIngredient = { tag: string };
export type Ingredient = ItemIngredient | TagIngredient | Ingredients;
export interface Ingredients extends Array<Ingredient> {}

/**
 * Converts stacks into ingredients. A `Stack` is more readable
 * and easier to write, but Minecraft only understands `Ingredient`.
 */
export function stacksIntoIngredients(stacks: Stack): Ingredients {
  let result: Ingredients = [];
  if (!Array.isArray(stacks)) {
    return stacksIntoIngredients([stacks]);
  }

  for (const stack of stacks) {
    if (Array.isArray(stack)) {
      result.push(stacksIntoIngredients(stack));
      continue;
    }
    result = result.concat(
      Array(stack.count).fill({
        [stack.type]: stack.name,
      }),
    );
  }
  return result;
}

/**
 * Converts items and tags into ingredients. An `ItemOrTag` is more readable
 * and easier to write, but Minecraft only understands `Ingredient`.
 */
export function itemOrTagIntoIngredient(xs: ItemOrTag[]): Ingredient[];
export function itemOrTagIntoIngredient(x: ItemOrTag): Ingredient;
export function itemOrTagIntoIngredient(
  xs: ItemOrTag | ItemOrTag[],
): Ingredient | Ingredient[] {
  const toIngredient = (x: ItemOrTag): Ingredient =>
    x.type === "item" ? { item: x.name } : { tag: x.name };

  return Array.isArray(xs) ? xs.map(toIngredient) : toIngredient(xs);
}
