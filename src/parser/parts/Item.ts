import { Either, left, right } from "fp-ts/lib/Either";
import { getItems } from "../../data";
import { findClosestMatches } from "../../data/compare";
import { CONFIG } from "../../simple-recipe";

/** Represents a Minecraft item. */
export interface Item {
  type: "item";
  name: string;
}

const withNamespaceRegex = /^(\w+):(\w+)$/;
const withoutNamespaceRegex = /^(\w+)$/;

let items: Set<string>;

/**
 * If the namespace of an item happens to be `minecraft`, this
 * function checks whether an item like that exists in Minecraft.
 * If not, it assumes the name was misspelled and tries to offer
 * helpful suggestions.
 */
function validateItem(namespace: string, name: string): Either<string, Item> {
  // Defer the initialization of items to avoid CONFIG
  // being undefined.
  if (!items) {
    items = getItems(CONFIG.gameVersion);
  }

  if (namespace === "minecraft" && !items.has(name)) {
    const suggestions = findClosestMatches([...items], name, 3, 0.4).join(", ");
    const offerSuggestions = suggestions.length
      ? ` Perhaps you wanted one of the following: ${suggestions}?`
      : "";
    return left(`Item "${name}" does not exist.${offerSuggestions}`);
  }

  return right({
    type: "item",
    name: namespace + ":" + name,
  });
}

/**
 * Expects a string in the shape `name` or `namespace:name`.
 * In the first form, namespace becomes `minecraft` by default.
 * If the namespace is `minecraft`, it also checks if such an
 * item actually exists inside Minecraft.
 */
export function parseItem(x: unknown): Either<string, Item> {
  if (typeof x !== "string") {
    return left("Expected a string.");
  }

  if (x === "") {
    return left("An Item cannot be an empty string.");
  }

  if (x.startsWith("+")) {
    return left("An Item may not begin with a +.");
  }

  let match = x.match(withNamespaceRegex);
  if (match) {
    return validateItem(match[1], match[2]);
  }

  match = x.match(withoutNamespaceRegex);
  if (match) {
    return validateItem("minecraft", match[1]);
  }

  return left("Invalid Item format was provided.");
}
