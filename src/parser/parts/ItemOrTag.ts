import { array } from "fp-ts/lib/Array";
import { Either, either, isLeft, isRight, left } from "fp-ts/lib/Either";
import { Item, parseItem } from "./Item";
import { parseTag, Tag } from "./Tag";

export type ItemOrTag = Item | Tag;
export type ItemOrTags = ItemOrTag[];

/**
 * Expects either an `Item` or a `Tag`.
 */
export function parseItemOrTag(x: unknown): Either<string, ItemOrTag> {
  const itemAttempt = parseItem(x);
  const tagAttempt = parseTag(x);

  if (isLeft(itemAttempt) && isLeft(tagAttempt)) {
    return left(
      "Expected either an Item or a Tag, but got neither:\n" +
        `  - Item: ${itemAttempt.left}\n` +
        `  - Tag: ${tagAttempt.left}`,
    );
  }

  if (isRight(itemAttempt)) {
    return itemAttempt;
  }

  return tagAttempt;
}

/**
 * Expects either a single `Item` or `Tag`, or an array of the two.
 */
export function parseItemOrTags(xs: unknown): Either<string, ItemOrTags> {
  if (!Array.isArray(xs)) {
    return either.map(parseItemOrTag(xs), self => [self]);
  }

  if (xs.length === 0) {
    return left("The array may not be empty.");
  }

  const errors = xs.map((x, i) => {
    return either.mapLeft(parseItemOrTag(x), msg => `[${i}] ${msg}`);
  });

  return array.traverse(either)(errors, x => x);
}
