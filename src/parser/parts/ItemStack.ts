import { Either, either, left } from "fp-ts/lib/Either";
import { parseItem } from "./Item";

/**
 * Represents a stack of items.
 */
export interface ItemStack {
  type: "item";
  /** An integer from 1 to 64. */
  count: number;
  name: string;
}

/**
 * Expects a string in the form `count Item`.
 */
export function parseItemStack(x: unknown): Either<string, ItemStack> {
  if (typeof x !== "string") {
    return left("An ItemStack must be a string.");
  }

  if (x === "") {
    return left("An ItemStack may not be an empty string.");
  }

  const tokens = x.split(" ");
  if (tokens.length !== 2) {
    return left("Invalid ItemStack provided.");
  }

  const count = Number(tokens[0]);
  if (
    Number.isNaN(count) ||
    count <= 0 ||
    count > 64 ||
    !Number.isInteger(count)
  ) {
    return left("Invalid count in ItemStack. Must be an integer from 1 to 64.");
  }

  const item = parseItem(tokens[1]);

  return either.map(item, x => ({
    type: "item",
    count,
    name: x.name,
  }));
}
