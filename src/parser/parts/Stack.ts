import { array } from "fp-ts/lib/Array";
import { Either, either, isLeft, isRight, left } from "fp-ts/lib/Either";
import { ItemStack, parseItemStack } from "./ItemStack";
import { parseTagStack, TagStack } from "./TagStack";

export type Stack = ItemStack | TagStack | Stacks;
export interface Stacks extends Array<Stack> {}

/**
 * Expects either an `ItemStack` or a `TagStack`.
 */
export function parseStack(xs: unknown): Either<string, Stack> {
  if (Array.isArray(xs)) {
    return parseStacks(xs);
  }

  const itemStackAttempt = parseItemStack(xs);
  const tagStackAttempt = parseTagStack(xs);

  if (isLeft(itemStackAttempt) && isLeft(tagStackAttempt)) {
    return left(
      "Expected either an ItemStack or a TagStack, but got neither:\n" +
        `  - ItemStack: ${itemStackAttempt.left}\n` +
        `  - TagStack: ${tagStackAttempt.left}`,
    );
  }

  if (isRight(itemStackAttempt)) {
    return itemStackAttempt;
  }

  return tagStackAttempt;
}

/**
 * Expects a single `ItemStack` or `TagStack`, or an array of those.
 */
function parseStacks(xs: unknown[]): Either<string, Stacks> {
  if (xs.length === 0) {
    return left("The array may not be empty.");
  }

  const errors = xs.map((x, i) => {
    return either.mapLeft(parseStack(x), msg => `[${i}] ${msg}`);
  });

  return array.traverse(either)(errors, x => x);
}
