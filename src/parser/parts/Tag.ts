import { Either, left, right } from "fp-ts/lib/Either";

/** Represents a Minecraft tag. */
export interface Tag {
  type: "tag";
  name: string;
}

/**
 * Forms a `Tag` from a `namespace` and a `name`.
 */
function validateTag(namespace: string, name: string): Either<string, Tag> {
  return right({
    type: "tag",
    name: namespace + ":" + name,
  });
}

const withNamespaceRegex = /^\+(\w+):(\w+)$/;
const withoutNamespaceRegex = /^\+(\w+)$/;

/**
 * Expects a string in the form `+name` or `+namespace:name`.
 * In the first form, the namespace is `minecraft` by default.
 */
export function parseTag(x: unknown): Either<string, Tag> {
  if (typeof x !== "string") {
    return left("Expected a string.");
  }

  if (x === "") {
    return left("A Tag cannot be an empty string.");
  }

  if (!x.startsWith("+")) {
    return left("A Tag must begin with a +.");
  }

  let match = x.match(withNamespaceRegex);
  if (match) {
    return validateTag(match[1], match[2]);
  }

  match = x.match(withoutNamespaceRegex);
  if (match) {
    return validateTag("minecraft", match[1]);
  }

  return left("Invalid Item format was provided.");
}
