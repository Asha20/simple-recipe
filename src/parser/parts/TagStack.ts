import { Either, either, left } from "fp-ts/lib/Either";
import { parseTag } from "./Tag";

/**
 * Represents a stack of tags.
 */
export interface TagStack {
  type: "tag";
  /** An integer from 1 to 64. */
  count: number;
  name: string;
}

/**
 * Expects a string in the form `count Tag`.
 */
export function parseTagStack(x: unknown): Either<string, TagStack> {
  if (typeof x !== "string") {
    return left("A TagStack must be a string.");
  }

  if (x === "") {
    return left("A TagStack may not be an empty string.");
  }

  const tokens = x.split(" ");
  if (tokens.length !== 2) {
    return left("Invalid TagStack provided.");
  }

  const count = Number(tokens[0]);
  if (
    Number.isNaN(count) ||
    count <= 0 ||
    count > 64 ||
    !Number.isInteger(count)
  ) {
    return left("Invalid count in TagStack. Must be an integer from 1 to 64.");
  }

  const tag = parseTag(tokens[1]);

  return either.map(tag, x => ({
    type: "tag",
    count,
    name: x.name,
  }));
}
