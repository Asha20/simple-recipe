import { sequenceT } from "fp-ts/lib/Apply";
import { either, Either, map, mapLeft } from "fp-ts/lib/Either";
import { pipe } from "fp-ts/lib/pipeable";
import { BaseRecipe } from ".";
import { recipeError, RecipeError, recipeErrorE } from "../index";
import {
  Ingredient,
  itemOrTagIntoIngredient,
  parseItem,
  parseItemOrTags,
} from "../parts/index";

type CookingType = "blasting" | "campfire_cooking" | "smelting" | "smoking";

interface Cooking<T extends CookingType> {
  type: T;
  ingredient: Ingredient[];
  result: string;
  experience: number;
  cookingtime?: number;
}

export type Smelting = Cooking<"smelting">;
export type CampfireCooking = Cooking<"campfire_cooking">;
export type Blasting = Cooking<"blasting">;
export type Smoking = Cooking<"smoking">;

export type AllCooking = Smelting | CampfireCooking | Blasting | Smoking;

/**
 * Expects input in the form:
 *
 * ```ts
 * type Input = {
 *   ingredients: ItemOrTag | ItemOrTag[];
 *   experience: number;
 *   cookingtime?: number;
 *   result: Item;
 * };
 * ```
 */
function parseCooking<T extends CookingType>(type: T) {
  return (x: BaseRecipe<T>): Either<RecipeError, Cooking<T>> => {
    const parsedIngredients = pipe(
      x.ingredients,
      parseItemOrTags,
      map(x => itemOrTagIntoIngredient(x)),
      mapLeft(x => recipeError(x, "ingredients")),
    );

    const parsedResult = pipe(
      parseItem(x.result),
      mapLeft(x => recipeError(x, "result")),
    );

    if (typeof x.experience !== "number") {
      return recipeErrorE("Expected a number.", "experience");
    }

    if (x.experience <= 0) {
      return recipeErrorE("Expected a positive number.", "experience");
    }

    if (x.cookingtime !== undefined) {
      if (typeof x.cookingtime !== "number") {
        return recipeErrorE("Expected a number.", "cookingtime");
      }

      if (x.cookingtime <= 0 || !Number.isInteger(x.cookingtime)) {
        return recipeErrorE("Expected a positive integer.", "cookingtime");
      }
    }

    return either.map(
      sequenceT(either)(parsedIngredients, parsedResult),
      ([ingredients, result]) => {
        const output: Cooking<T> = {
          type,
          ingredient: ingredients,
          result: result.name,
          experience: x.experience,
        };

        if (x.cookingtime !== undefined) {
          output.cookingtime = x.cookingtime;
        }
        return output;
      },
    );
  };
}

export const parseSmelting = parseCooking("smelting");
export const parseCampfireCooking = parseCooking("campfire_cooking");
export const parseBlasting = parseCooking("blasting");
export const parseSmoking = parseCooking("smoking");
