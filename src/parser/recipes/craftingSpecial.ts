import { Either, right } from "fp-ts/lib/Either";
import { BaseRecipe } from ".";
import { RecipeError } from "..";

type CraftingSpecialType =
  | "crafting_special_armordye"
  | "crafting_special_bannerduplicate"
  | "crafting_special_bookcloning"
  | "crafting_special_firework_rocket"
  | "crafting_special_firework_star"
  | "crafting_special_firework_star_fade"
  | "crafting_special_mapcloning"
  | "crafting_special_mapextending"
  | "crafting_special_repairitem"
  | "crafting_special_shielddecoration"
  | "crafting_special_shulkerboxcoloring"
  | "crafting_special_tippedarrow"
  | "crafting_special_suspiciousstew";

interface CraftingSpecial<T extends CraftingSpecialType> {
  type: T;
}

type DistributeCraftingSpecial<T> = T extends CraftingSpecialType
  ? CraftingSpecial<T>
  : never;

export type AllCraftingSpecial = DistributeCraftingSpecial<CraftingSpecialType>;

/**
 * Special crafting recipes don't require additional properties
 * beside the `type`.
 */
export function parseCraftingSpecial<T extends CraftingSpecialType>(
  x: BaseRecipe<T>,
): Either<RecipeError, CraftingSpecial<T>> {
  return right({
    type: x.type,
  });
}
