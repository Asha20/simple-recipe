import {
  AllCooking,
  Blasting,
  CampfireCooking,
  parseBlasting,
  parseCampfireCooking,
  parseSmelting,
  parseSmoking,
  Smelting,
  Smoking,
} from "./cooking";
import { AllCraftingSpecial, parseCraftingSpecial } from "./craftingSpecial";
import { parseShapedCrafting, ShapedCrafting } from "./shapedCrafting";
import { parseShapelessCrafting, ShapelessCrafting } from "./shapelessCrafting";
import { parseStonecutting, Stonecutting } from "./stonecutting";

export interface BaseRecipe<T extends string = string> {
  type: T;
  [key: string]: any;
}

export type Recipe =
  | ShapedCrafting
  | ShapelessCrafting
  | Stonecutting
  | AllCooking
  | AllCraftingSpecial;

export {
  ShapedCrafting,
  parseShapedCrafting,
  ShapelessCrafting,
  parseShapelessCrafting,
  Blasting,
  parseBlasting,
  CampfireCooking,
  parseCampfireCooking,
  Smelting,
  parseSmelting,
  Smoking,
  parseSmoking,
  Stonecutting,
  parseStonecutting,
  AllCraftingSpecial,
  parseCraftingSpecial,
};
