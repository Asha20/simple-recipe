import { either, Either, isLeft, map, mapLeft } from "fp-ts/lib/Either";
import { pipe } from "fp-ts/lib/pipeable";
import { BaseRecipe } from ".";
import { RecipeError, recipeError, recipeErrorE } from "..";
import {
  Ingredient,
  itemOrTagIntoIngredient,
  parseItemOrTags,
  parseItemStack,
} from "../parts/index";
import { isObject } from "../util";

export interface ShapedCrafting {
  type: "crafting_shaped";
  pattern: string[];
  key: Record<string, Ingredient[]>;
  result: {
    count: number;
    item: string;
  };
}

/**
 * Expects input in the form:
 *
 * ```ts
 * type Input = {
 *   pattern: string[];
 *   key: Record<string, ItemOrTag | ItemOrTag[]>;
 *   result: ItemStack;
 * }
 * ```
 */
export function parseShapedCrafting(
  x: BaseRecipe<"crafting_shaped">,
): Either<RecipeError, ShapedCrafting> {
  if (
    !Array.isArray(x.pattern) ||
    x.pattern.some(row => typeof row !== "string")
  ) {
    return recipeErrorE("Expected an array of strings.", "pattern");
  }

  const pattern = x.pattern as string[];

  if (
    pattern.length === 0 ||
    pattern.length > 3 ||
    pattern.some(row => row.length === 0 || row.length > 3)
  ) {
    return recipeErrorE("Expected array to be between 1x1 and 3x3.", "pattern");
  }

  if (!isObject(x.key)) {
    return recipeErrorE("Expected an object.", "key");
  }

  const keys = x.key;

  type Keys = Record<string, Ingredient[]>;

  const parsedKeys: Keys = {};
  for (const key of Object.keys(keys)) {
    const itemOrTags = parseItemOrTags(keys[key]);
    const ingredients = pipe(
      itemOrTags,
      map(x => itemOrTagIntoIngredient(x)),
      mapLeft(x => recipeError(x, `ingredients.${key}`)),
    );

    if (isLeft(ingredients)) {
      return ingredients;
    }

    parsedKeys[key] = ingredients.right;
  }

  const parsedResult = pipe(
    parseItemStack(x.result),
    mapLeft(x => recipeError(x, "result")),
  );

  return either.map(parsedResult, result => ({
    type: x.type,
    pattern: x.pattern,
    key: parsedKeys,
    result: {
      count: result.count,
      item: result.name,
    },
  }));
}
