import { sequenceT } from "fp-ts/lib/Apply";
import { chain, either, Either, left, mapLeft, right } from "fp-ts/lib/Either";
import { pipe } from "fp-ts/lib/pipeable";
import { BaseRecipe } from ".";
import { RecipeError, recipeError } from "..";
import {
  Ingredient,
  parseItemStack,
  parseStack,
  stacksIntoIngredients,
} from "../parts/index";

export interface ShapelessCrafting {
  type: "crafting_shapeless";
  ingredients: Ingredient[];
  result: {
    count: number;
    item: string;
  };
}

/** Crafting is done in a 3x3 grid, so 9 is the maximum number of ingredients. */
const MAX_ALLOWED_INGREDIENTS = 9;

/**
 * Expects input in the form:
 *
 * ```ts
 * type Input = {
 *   ingredients: Stack | Stack[];
 *   result: ItemStack;
 * }
 * ```
 */
export function parseShapelessCrafting(
  x: BaseRecipe<"crafting_shapeless">,
): Either<RecipeError, ShapelessCrafting> {
  const parsedIngredients = pipe(
    x.ingredients,
    parseStack,
    chain(stacks => {
      const ingredients = stacksIntoIngredients(stacks);
      return ingredients.length > MAX_ALLOWED_INGREDIENTS
        ? left("Too many ingredients were provided.")
        : right(ingredients);
    }),
    mapLeft(x => recipeError(x, "ingredients")),
  );

  const parsedResult = pipe(
    parseItemStack(x.result),
    mapLeft(x => recipeError(x, "result")),
  );

  return either.map(
    sequenceT(either)(parsedIngredients, parsedResult),
    ([ingredients, result]) => ({
      type: x.type,
      ingredients,
      result: {
        count: result.count,
        item: result.name,
      },
    }),
  );
}
