import { sequenceT } from "fp-ts/lib/Apply";
import { either, Either, map, mapLeft } from "fp-ts/lib/Either";
import { pipe } from "fp-ts/lib/pipeable";
import { BaseRecipe } from ".";
import { RecipeError, recipeError } from "..";
import {
  Ingredient,
  itemOrTagIntoIngredient,
  parseItemOrTags,
  parseItemStack,
} from "../parts";

export interface Stonecutting {
  type: "stonecutting";
  ingredient: Ingredient[];
  result: string;
  count: number;
}

/**
 * Expects input in the form:
 *
 * ```ts
 * type Input = {
 *   ingredients: ItemOrTag | ItemOrTag[];
 *   result: ItemStack;
 * }
 * ```
 */
export function parseStonecutting(
  x: BaseRecipe<"stonecutting">,
): Either<RecipeError, Stonecutting> {
  const parsedIngredients = pipe(
    parseItemOrTags(x.ingredients),
    map(x => itemOrTagIntoIngredient(x)),
    mapLeft(x => recipeError(x, "ingredients")),
  );

  const parsedResult = pipe(
    parseItemStack(x.result),
    mapLeft(x => recipeError(x, "result")),
  );

  return either.map(
    sequenceT(either)(parsedIngredients, parsedResult),
    ([ingredients, result]) => ({
      type: x.type,
      ingredient: ingredients,
      // While other recipes contain the whole output inside
      // an object under "result", stonecutting recipes
      // store the item name as "result" and the count as "count".
      result: result.name,
      count: result.count,
    }),
  );
}
