import minimist from "minimist";
import * as path from "path";
import { fileCache, memoryCache, RecipeCache } from "./cache";
import { isSupportedVersion, supportedVersions } from "./data";
import { isDir } from "./fs-promise";
import { watchRecipes } from "./watcher";

/**
 * Version of the application. The `scripts/prebuild.js` script will synchronize
 * this value with the one from package.json.
 */
export const VERSION = "0.0.0";

function exitWithMessage(msg: string, code = 1) {
  console.log(msg);
  process.exit(code);
}

function getCache(type: string, root: string): RecipeCache | null {
  switch (type) {
    case "file":
      return fileCache(path.join(root, "cache.json"));
    case "memory":
      return memoryCache();
    default:
      return null;
  }
}

export const CONFIG = {
  verbosity: 2,
  root: "",
  gameVersion: "1.14",
};

async function main() {
  const help = `
Usage: simple-recipe [OPTION]... <directory>

Options:
  -h, --help                Displays this help message
  --version                 Displays the version of the program
  -v n, --verbosity=n       Sets the verbosity level
                              0 - Errors
                              1 (default) - Broad overviews of changes made
                              2 - File by file changes

  --cache={file|memory}     Sets the type of cache. Defaults to "file".
  -g v, --game-version=v    The version of MineCraft that you're writing
                            recipes for. Defaults to 1.14.
`;

  const options = minimist(process.argv.slice(2), {
    string: ["cache", "game-version", "_"],
    boolean: ["help", "version"],
    alias: {
      h: "help",
      v: "verbosity",
      g: "game-version",
    },
    default: {
      cache: "file",
      verbosity: CONFIG.verbosity,
    },
  });

  if (options.help) {
    return exitWithMessage(help, 0);
  }

  if (options.version) {
    return exitWithMessage(VERSION, 0);
  }

  if (options["game-version"] === undefined) {
    console.log(
      `No game version was provided; defaulting to ${CONFIG.gameVersion}.`,
    );
  } else {
    CONFIG.gameVersion = options["game-version"];
  }

  if (!isSupportedVersion(CONFIG.gameVersion)) {
    const supportedVersionsStr = supportedVersions.join(", ");
    return exitWithMessage(
      `Unsupported version "${
        CONFIG.gameVersion
      }"; supported versions are: ${supportedVersionsStr}`,
    );
  }

  const verbosity = Number(options.verbosity);
  if (
    Number.isNaN(verbosity) ||
    verbosity <= 0 ||
    !Number.isInteger(verbosity)
  ) {
    return exitWithMessage("Verbosity must be a positive integer.");
  }

  CONFIG.verbosity = verbosity;

  const root = String(options._[0]);
  if (!root) {
    return exitWithMessage("A root directory must be provided.");
  }
  const resolvedRoot = path.resolve(root);
  if (!(await isDir(resolvedRoot))) {
    return exitWithMessage(
      "Provided path either doesn't exist or is not a directory.",
    );
  }

  CONFIG.root = resolvedRoot;

  const cache = getCache(options.cache, root);
  if (!cache) {
    return exitWithMessage(
      'Invalid cache type was provided. Expected either "file" or "memory".',
    );
  }

  console.log(`Now watching for file changes in "${resolvedRoot}":`);
  const watcher = watchRecipes(root, cache);
}

if (!module.parent) {
  main();
}
