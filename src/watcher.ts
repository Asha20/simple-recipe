import chokidar from "chokidar";
import { isLeft, isRight } from "fp-ts/lib/Either";
import path from "path";
import { RecipeCache } from "./cache";
import { rmDir, unlink } from "./fs-promise";
import * as logger from "./logger";
import { parseFile, RecipeError } from "./parser";
import { writeRecipes } from "./writer";

interface UpdateData {
  totalRecipes: number;
  fromCache: number;
  validRecipes: number;
  invalidRecipes: number;
  totalFiles: number;
  updatedFiles: number;
}

function indent(n: number, str: string) {
  return str
    .split("\n")
    .map(x => " ".repeat(n) + x)
    .join("\n");
}

function reportError(file: string, errors: RecipeError[]) {
  if (!errors.length) {
    return "";
  }

  let output = `Found ${errors.length} errors in file "${file}.yml": \n\n`;

  for (const { message, name, index, path } of errors) {
    const strName = name ? `, "${name}",` : ",";
    const recipeName = index + strName;
    const stringPath = ["recipe", ...path].join(".");
    let stringified = indent(
      2,
      `Recipe ${recipeName} at ${stringPath}:\n\n` + indent(2, message),
    );
    output += stringified + "\n\n";
  }

  return output;
}

function reportErrors(errors: Record<string, RecipeError[]>) {
  const output = Object.entries(errors)
    .map(x => reportError(...x))
    .join("\n\n");

  return output.trim().length ? output : "";
}

function generateReport(
  data: UpdateData,
  errors: Record<string, RecipeError[]>,
) {
  const recipesChanged = data.validRecipes - data.fromCache;
  const recipesToChange = data.totalRecipes - data.fromCache;

  if (recipesToChange === 0) {
    return "";
  }
  const reportedErrors = reportErrors(errors);

  return (
    `Successfully updated ${recipesChanged} recipes out of ${recipesToChange}.\n\n` +
    reportedErrors
  );
}

export function watchRecipes(root: string, cache: RecipeCache) {
  const watcher = chokidar.watch("**/*.yml", {
    cwd: root,
  });

  async function onAddOrChange(files: string[]) {
    const errors: Record<string, RecipeError[]> = {};
    const data = {
      totalRecipes: 0,
      fromCache: 0,
      validRecipes: 0,
      invalidRecipes: 0,
      totalFiles: files.length,
      updatedFiles: 0,
    };

    for (const yamlPath of files) {
      const absPath = path.join(root, yamlPath + ".yml");
      const recipes = await parseFile(absPath);

      const validRecipes = recipes.filter(isRight).map(x => x.right);
      errors[yamlPath] = recipes.filter(isLeft).map(x => x.left);

      data.totalRecipes += recipes.length;
      data.validRecipes += validRecipes.length;

      const oldFiles = Object.keys(cache.local.hashes).filter(x =>
        x.startsWith(yamlPath),
      );

      const results = await writeRecipes(root, yamlPath, validRecipes, cache);

      for (const result of results.filter(isLeft)) {
        data.invalidRecipes += 1;
        logger.error(result.left);
      }

      const validResults = results.filter(isRight).map(x => x.right);

      const filesFromCache = validResults.reduce(
        (acc, x) => acc + Number(x.fromCache),
        0,
      );

      data.fromCache += filesFromCache;

      if (filesFromCache === 0) {
        data.updatedFiles += 1;
      }

      // Delete all recipes that used to exist inside
      // the YAML file before, but have now been removed.
      const resultsSet = new Set(validResults.map(x => x.file));
      for (const json of oldFiles) {
        if (!resultsSet.has(json)) {
          await unlink(path.join(root, json));
          delete cache.local.hashes[json];
        }
      }
      await cache.save();
    }

    const report = generateReport(data, errors);
    if (report) {
      logger.clear();
      logger.important(report);
    }
  }

  async function onUnlink(files: string[]) {
    const data = {
      recipeCount: 0,
      updatedFiles: 0,
    };

    for (const yamlPath of files) {
      const keysToDelete = Object.keys(cache.local.hashes).filter(json =>
        json.startsWith(yamlPath),
      );

      data.recipeCount += keysToDelete.length;
      data.updatedFiles += 1;

      await Promise.all(
        keysToDelete.map(async json => {
          delete cache.local.hashes[json];
          await unlink(path.join(root, json));
        }),
      );

      await cache.save();
      await rmDir(path.join(root, yamlPath));
    }

    logger.important(
      `Removed ${data.recipeCount} recipes from ${data.updatedFiles} files.`,
    );
  }

  async function onUnlinkDir(dirs: string[]) {
    for (const dir of dirs) {
      Object.keys(cache.local.hashes)
        .filter(json => json.startsWith(dir))
        .forEach(json => {
          delete cache.local.hashes[json];
        });

      await cache.save();
    }
  }

  const removeYmlSuffix = <T>(f: (yaml: string[]) => T) => (yaml: string[]) => {
    return f(yaml.map(x => x.slice(0, -4)));
  };

  type ChokidarEvent = "add" | "change" | "unlink" | "unlinkDir";
  interface Operation {
    type: ChokidarEvent;
    name: string;
  }

  function handleBatch(buffer: Operation[]) {
    const handlers = {
      add: removeYmlSuffix(onAddOrChange),
      change: removeYmlSuffix(onAddOrChange),
      unlink: removeYmlSuffix(onUnlink),
      unlinkDir: onUnlinkDir,
    };

    const groupedEvents = buffer.reduce<Record<ChokidarEvent, string[]>>(
      (acc, { type, name }) => {
        acc[type].push(name);
        return acc;
      },
      {
        add: [],
        change: [],
        unlink: [],
        unlinkDir: [],
      },
    );

    for (const [type, files] of Object.entries(groupedEvents)) {
      if (files.length) {
        handlers[type as ChokidarEvent](files);
      }
    }
  }

  const BATCH_TIMER = 100;
  let buffer: Operation[] = [];
  let bufferTimer = 0;
  function batch(type: ChokidarEvent, name: string) {
    clearTimeout(bufferTimer);
    buffer.push({ type, name });
    bufferTimer = (setTimeout(() => {
      handleBatch(buffer);
      buffer = [];
    }, BATCH_TIMER) as unknown) as number;
  }

  watcher.on("all", batch);

  return watcher;
}
