import { Either, right } from "fp-ts/lib/Either";
import hash from "object-hash";
import * as path from "path";
import { RecipeCache } from "./cache";
import { mkDir, writeFile } from "./fs-promise";
import * as logger from "./logger";
import { RecipeWithMeta } from "./parser/index";

export interface WriteInfo {
  fromCache: boolean;
  file: string;
}

export async function writeRecipe(
  rootPath: string,
  source: string,
  recipeWithMeta: RecipeWithMeta,
  cache: RecipeCache,
): Promise<Either<string, WriteInfo>> {
  const { meta, ...recipe } = recipeWithMeta;

  const recipeSubfolder = path.join(rootPath, source);

  await mkDir(recipeSubfolder);

  const recipeHash = hash(recipe);
  const jsonFilename = (meta.name || recipeHash.slice(0, 10)) + ".json";
  const absDestination = path.join(recipeSubfolder, jsonFilename);
  const destination = path.relative(rootPath, absDestination);

  const matchesCache = cache.local.hashes[destination] === recipeHash;
  if (!matchesCache) {
    await writeFile(absDestination, JSON.stringify(recipe, null, 4));
    logger.log("Updating: " + destination);
    cache.local.hashes[destination] = recipeHash;
    await cache.save();
  } else {
    logger.log("Skipping to due cache: " + destination);
  }

  return right({ file: destination, fromCache: matchesCache });
}

export async function writeRecipes(
  rootPath: string,
  source: string,
  recipesWithMeta: RecipeWithMeta[],
  cache: RecipeCache,
): Promise<Either<string, WriteInfo>[]> {
  return await Promise.all(
    recipesWithMeta.map(recipe => {
      return writeRecipe(rootPath, source, recipe, cache);
    }),
  );
}
