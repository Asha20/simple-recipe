import { isRight, right } from "fp-ts/lib/Either";
import {
  itemOrTagIntoIngredient,
  ItemOrTags,
  parseItem,
  parseItemOrTag,
  parseItemOrTags,
  parseItemStack,
  parseStack,
  parseTag,
  parseTagStack,
  Stacks,
  stacksIntoIngredients,
} from "../../src/parser/parts";

test("Parsing Item", () => {
  const tryItem = (x: unknown) => isRight(parseItem(x));

  expect(tryItem(1), "Must be a string").toBeFalsy();
  expect(tryItem(""), "Cannot be an empty string").toBeFalsy();
  expect(tryItem("+foo"), "Can't start with a +").toBeFalsy();
  expect(tryItem("@foo!"), "Can't contain invalid character").toBeFalsy();
  expect(tryItem("a:b:c"), "Invalid format").toBeFalsy();
  expect(tryItem("foobar"), "foo isn't a Minecraft item").toBeFalsy();
  expect(tryItem("bar"), "bar isn't a Minecraft item").toBeFalsy();

  expect(tryItem("minecraft:apple"), "apple is a Minecraft item").toBeTruthy();
  expect(
    tryItem("nether_star"),
    "nether_star is a Minecraft item",
  ).toBeTruthy();

  expect(parseItem("diamond"), "diamond item without namespace").toEqual(
    right({
      type: "item",
      name: "minecraft:diamond",
    }),
  );

  expect(parseItem("foo:bar"), "bar item with namespace foo").toEqual(
    right({
      type: "item",
      name: "foo:bar",
    }),
  );
});

test("Parsing Tag", () => {
  const tryTag = (x: unknown) => isRight(parseTag(x));

  expect(tryTag(1), "Must be a string").toBeFalsy();
  expect(tryTag(""), "Cannot be an empty string").toBeFalsy();
  expect(tryTag("foo"), "Must start with a +").toBeFalsy();
  expect(tryTag("+@foo!"), "Can't contain invalid character").toBeFalsy();
  expect(tryTag("+a:b:c"), "Invalid format").toBeFalsy();

  expect(parseTag("+foo"), "diamond tag without namespace").toEqual(
    right({
      type: "tag",
      name: "minecraft:foo",
    }),
  );

  expect(parseTag("+foo:bar"), "bar tag with namespace foo").toEqual(
    right({
      type: "tag",
      name: "foo:bar",
    }),
  );
});

test("Parsing ItemOrTag(s)", () => {
  const tryItemOrTag = (x: unknown) => isRight(parseItemOrTag(x));

  expect(parseItemOrTag("apple"), "Parses an Item").toEqual(
    right({
      type: "item",
      name: "minecraft:apple",
    }),
  );
  expect(parseItemOrTag("+foo"), "Parses a Tag").toEqual(
    right({
      type: "tag",
      name: "minecraft:foo",
    }),
  );

  expect(tryItemOrTag(1), "Errors if not Item or Tag").toBeFalsy();

  expect(
    parseItemOrTags(["apple", "+anvil"]),
    "Mixed array of Item and Tag",
  ).toEqual(
    right([
      { type: "item", name: "minecraft:apple" },
      { type: "tag", name: "minecraft:anvil" },
    ]),
  );

  expect(parseItemOrTags("apple"), "Single Item or Tag into an array").toEqual(
    right([{ type: "item", name: "minecraft:apple" }]),
  );
});

test("Parsing ItemStack", () => {
  const tryItemStack = (x: unknown) => isRight(parseItemStack(x));

  expect(tryItemStack(1), "Must be a string").toBeFalsy();
  expect(tryItemStack(""), "Cannot be an empty string").toBeFalsy();
  expect(tryItemStack("one two three"), "Must have two tokens").toBeFalsy();
  expect(
    tryItemStack("hello apple"),
    "First token must be a number",
  ).toBeFalsy();
  expect(tryItemStack("0 apple"), "Count must be larger than zero").toBeFalsy();
  expect(
    tryItemStack("65 apple"),
    "Count must be no larger than 64",
  ).toBeFalsy();
  expect(tryItemStack("1.2 apple"), "Count must be an integer").toBeFalsy();

  expect(parseItemStack("1 apple"), "An ItemStack of one apple").toEqual(
    right({
      type: "item",
      name: "minecraft:apple",
      count: 1,
    }),
  );
});

test("Parsing TagStack", () => {
  const tryTagStack = (x: unknown) => isRight(parseTagStack(x));

  expect(tryTagStack(1), "Must be a string").toBeFalsy();
  expect(tryTagStack(""), "Cannot be an empty string").toBeFalsy();
  expect(tryTagStack("one two three"), "Must have two tokens").toBeFalsy();
  expect(
    tryTagStack("hello apple"),
    "First token must be a number",
  ).toBeFalsy();
  expect(tryTagStack("0 apple"), "Count must be larger than zero").toBeFalsy();
  expect(
    tryTagStack("65 apple"),
    "Count must be no larger than 64",
  ).toBeFalsy();
  expect(tryTagStack("1.2 apple"), "Count must be an integer").toBeFalsy();

  expect(parseTagStack("1 +anvil"), "A TagStack of one anvil").toEqual(
    right({
      type: "tag",
      name: "minecraft:anvil",
      count: 1,
    }),
  );
});

test("Parsing Stack(s)", () => {
  const tryStack = (x: unknown) => isRight(parseStack(x));

  expect(parseStack("1 apple"), "Parses an ItemStack").toEqual(
    right({
      type: "item",
      count: 1,
      name: "minecraft:apple",
    }),
  );
  expect(parseStack("2 +foo"), "Parses a TagStack").toEqual(
    right({
      type: "tag",
      count: 2,
      name: "minecraft:foo",
    }),
  );

  expect(tryStack(1), "Errors if not ItemStack or TagStack").toBeFalsy();

  expect(
    parseStack(["1 apple", "2 +anvil"]),
    "Mixed array of ItemStack and TagStack",
  ).toEqual(
    right([
      { type: "item", count: 1, name: "minecraft:apple" },
      { type: "tag", count: 2, name: "minecraft:anvil" },
    ]),
  );

  expect(parseStack("10 apple"), "Single Item or Tag into an array").toEqual(
    right({ type: "item", count: 10, name: "minecraft:apple" }),
  );
});

test("Converting ItemOrTag and Stack into Ingredient", () => {
  const stacks: Stacks = [
    { type: "item", count: 1, name: "minecraft:apple" },
    { type: "tag", count: 2, name: "minecraft:anvil" },
  ];
  const itemOrTags: ItemOrTags = [
    { type: "item", name: "minecraft:diamond" },
    { type: "tag", name: "foo:bar" },
  ];

  expect(stacksIntoIngredients(stacks), "Stacks into Ingredients").toEqual([
    { item: "minecraft:apple" },
    { tag: "minecraft:anvil" },
    { tag: "minecraft:anvil" },
  ]);

  expect(
    itemOrTagIntoIngredient(itemOrTags),
    "ItemOrTags into Ingredients",
  ).toEqual([{ item: "minecraft:diamond" }, { tag: "foo:bar" }]);
});
