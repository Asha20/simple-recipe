import { right } from "fp-ts/lib/Either";
import {
  parseShapedCrafting,
  parseShapelessCrafting,
  parseSmelting,
  parseStonecutting,
  ShapedCrafting,
  ShapelessCrafting,
  Smelting,
  Stonecutting,
} from "../../src/parser/recipes/index";

test("Parse shapeless crafting recipe", () => {
  const shapelessRecipe1: ShapelessCrafting = {
    type: "crafting_shapeless",
    ingredients: [
      { item: "minecraft:sand" },
      { item: "minecraft:sand" },
      { item: "minecraft:gunpowder" },
      { item: "minecraft:gunpowder" },
    ],
    result: {
      count: 1,
      item: "minecraft:tnt",
    },
  };
  expect(
    parseShapelessCrafting({
      type: "crafting_shapeless",
      ingredients: ["2 sand", "2 gunpowder"],
      result: "1 tnt",
    }),
  ).toStrictEqual(right(shapelessRecipe1));

  const shapelessRecipe2: ShapelessCrafting = {
    type: "crafting_shapeless",
    ingredients: [
      { tag: "minecraft:wool" },
      { tag: "minecraft:wool" },
      { item: "foo:black_dye" },
    ],
    result: {
      count: 2,
      item: "minecraft:black_wool",
    },
  };
  expect(
    parseShapelessCrafting({
      type: "crafting_shapeless",
      ingredients: ["1 +wool", "1 +minecraft:wool", "1 foo:black_dye"],
      result: "2 black_wool",
    }),
  ).toStrictEqual(right(shapelessRecipe2));
});

test("Parse shaped crafting recipe", () => {
  const shapedRecipe1: ShapedCrafting = {
    type: "crafting_shaped",
    key: {
      S: [{ item: "minecraft:stone" }],
      W: [{ item: "minecraft:stick" }],
    },
    pattern: ["SSS", " W ", " W "],
    result: {
      count: 1,
      item: "foo:stone_pickaxe",
    },
  };
  expect(
    parseShapedCrafting({
      type: "crafting_shaped",
      pattern: shapedRecipe1.pattern,
      key: {
        S: "stone",
        W: "stick",
      },
      result: "1 foo:stone_pickaxe",
    }),
  ).toStrictEqual(right(shapedRecipe1));

  const shapedRecipe2: ShapedCrafting = {
    type: "crafting_shaped",
    key: {
      A: [{ tag: "minecraft:anvil" }, { tag: "minecraft:dye" }],
      S: [{ item: "minecraft:stone" }],
    },
    pattern: ["AAA", "ASA", "AAA"],
    result: {
      count: 1,
      item: "minecraft:diamond",
    },
  };
  expect(
    parseShapedCrafting({
      type: "crafting_shaped",
      pattern: shapedRecipe2.pattern,
      key: {
        A: ["+anvil", "+dye"],
        S: "stone",
      },
      result: "1 diamond",
    }),
  ).toStrictEqual(right(shapedRecipe2));
});

// Blasting, smelting, campfire cooking and smoking all fall
// under cooking recipes and only differ by their type, so
// only smelting is tested here.
test("Parse cooking recipe", () => {
  const cooking1: Smelting = {
    type: "smelting",
    ingredient: [
      { item: "minecraft:redstone" },
      { item: "minecraft:gold_ingot" },
    ],
    result: "minecraft:diamond",
    experience: 100,
  };

  expect(
    parseSmelting({
      type: "smelting",
      ingredients: ["redstone", "gold_ingot"],
      result: "diamond",
      experience: 100,
    }),
  ).toStrictEqual(right(cooking1));

  const cooking2: Smelting = {
    type: "smelting",
    ingredient: [{ tag: "minecraft:anvil" }, { item: "minecraft:apple" }],
    result: "foo:bar",
    experience: 5,
    cookingtime: 10,
  };

  expect(
    parseSmelting({
      type: "smelting",
      ingredients: ["+anvil", "minecraft:apple"],
      result: "foo:bar",
      experience: 5,
      cookingtime: 10,
    }),
  ).toStrictEqual(right(cooking2));
});

test("Parse stonecutting recipe", () => {
  const stonecutting1: Stonecutting = {
    type: "stonecutting",
    ingredient: [{ tag: "foo:stone" }, { item: "minecraft:andesite" }],
    count: 1,
    result: "minecraft:cobblestone",
  };

  expect(
    parseStonecutting({
      type: "stonecutting",
      ingredients: ["+foo:stone", "andesite"],
      result: "1 cobblestone",
    }),
  ).toStrictEqual(right(stonecutting1));
});
